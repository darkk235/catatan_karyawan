<?php

namespace App\Http\Controllers;

use App\Http\Requests\PerminssionRequest;
use App\Models\Permission;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PerimissionController extends Controller
{
    public function store(PerminssionRequest $request): RedirectResponse
    {
        Permission::create($request->validated());
        
        return Redirect::to('/profile');
    }

    public function update(PerminssionRequest $request): RedirectResponse
    {
        Permission::find($request->id)->update($request->validated());
        
        return Redirect::to('/profile');
    }

    public function destroy(Request $request): RedirectResponse
    {
        Permission::find($request->id)->delete();
        
        return Redirect::to('/profile');
    }
}
