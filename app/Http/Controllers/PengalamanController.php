<?php

namespace App\Http\Controllers;

use App\Http\Requests\PengalamanUpdateRequest;
use App\Models\Pengalaman;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PengalamanController extends Controller
{
    public function store(PengalamanUpdateRequest $request): RedirectResponse
    {
        Pengalaman::create($request->validated());
        
        return Redirect::to('/profile');
    }

    public function update(PengalamanUpdateRequest $request): RedirectResponse
    {
        Pengalaman::where('nik',$request->nik)->update($request->validated());
        
        return Redirect::to('/profile');
    }

    public function destroy(Request $request): RedirectResponse
    {
        Pengalaman::find($request->id)->delete();
        
        return Redirect::to('/profile');
    }
}
