<?php

namespace App\Http\Controllers\Perusahaan;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class PencarianController extends Controller
{
    public function index(Request $request): Response
    {
        return Inertia::render('Perusahaan/Hasil',[
            'hasil' => $this->pencarian($request)
        ]);
    }

    private function pencarian($request)
    {
        $query = User::with('pengalaman','catatan.perusahaan','catatan.permission');

        if ($request->kategori == "Karyawan") {
            $query->where('name', 'like', '%' . $request->pencarian . '%');
        } elseif ($request->kategori == "Jabatan") {
            $query->where('jabatan', 'like', '%' . $request->pencarian . '%');
        } elseif ($request->kategori == "Perusahaan") {
            $query->whereRelation('pengalaman','perusahaan', 'like', '%' . $request->pencarian . '%');
        } else {
            $query;
        }

        return $query->where('role', 'Karyawan')->get();
    }
}
