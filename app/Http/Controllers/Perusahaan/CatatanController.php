<?php

namespace App\Http\Controllers\Perusahaan;

use App\Http\Controllers\Controller;
use App\Http\Requests\PengalamanUpdateRequest;
use App\Models\Catatan;
use App\Models\Pengalaman;
use App\Models\Permission;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Response;

class CatatanController extends Controller
{
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'catatan' => 'required',
        ]);

        if ($request->id_catatan == "") {
            Catatan::create([
                'id_perusahaan' => Auth::user()->id,
                'catatan' => $request->catatan,
                'nik' => $request->nik
            ]);
        }else{
            Catatan::find($request->id_catatan)->update(['catatan' => $request->catatan]);
        }


        return Redirect::back();
    }

    public function destroy(Request $request): RedirectResponse
    {
        Pengalaman::find($request->id)->delete();
        
        return Redirect::to('/profile');
    }
}
