<?php

namespace App\Http\Controllers\Perusahaan;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\Catatan;
use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function index(Request $request): Response
    {
        return Inertia::render('Perusahaan/Index', [
            'mustVerifyEmail' => $request->user() instanceof MustVerifyEmail,
            'status' => session('status'),
            'catatan' => Catatan::with('user_nik')->where('id_perusahaan',Auth::user()->id)->get()
        ]);
    }

    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $file = $request->file('avatar');

        $data = [
            'nik' => $request->nik,
            'name' => $request->name,
            'email' => $request->email,
            'no_hp' => $request->no_hp,
            'username' => $request->username,
            'deskripsi' => $request->deskripsi
        ];

        if ($file) {
            $fileName = date('Ymdhis').'.'.$file->getClientOriginalExtension();
            $data += ['avatar' => url('avatar/'.$fileName)];
            $file->move('avatar',$fileName);
        }
       
        User::find(Auth::user()->id)->update($data);

        return Redirect::route('perusahaan.profile.index');
    }

    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validate([
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }
}
