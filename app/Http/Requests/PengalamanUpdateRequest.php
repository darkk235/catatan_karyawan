<?php

namespace App\Http\Requests;

use App\Models\Pengalaman;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PengalamanUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'nik' => ['required','numeric'],
            'perusahaan' => ['required', 'string'],
            'jabatan' => ['required','string'],
            'mulai_bekerja' => ['required','date'],
            'selesai_bekerja' => ['required','date'],
            'masih_aktif' => ['required'],
        ];
    }
}
