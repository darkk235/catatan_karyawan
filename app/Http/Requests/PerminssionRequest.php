<?php

namespace App\Http\Requests;

use App\Models\Pengalaman;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PerminssionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'nik' => ['required','numeric'],
            'id_perusahaan' => ['required'],
            'akses_view' => ['required','string'],
            'akses_edit' => ['required','string'],
        ];
    }
}
