<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Permission extends Model
{
    protected $table = "tb_permission";
    protected $guarded = ['id'];

    public function perusahaan(): HasOne
    {
        return $this->hasOne(User::class, 'id','id_perusahaan');
    }
}
