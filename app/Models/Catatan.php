<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Catatan extends Model
{
    protected $table = "tb_catatan";
    protected $guarded = ['id'];

    public function perusahaan(): HasOne
    {
        return $this->hasOne(User::class, 'id','id_perusahaan');
    }

    public function user_nik(): HasOne
    {
        return $this->hasOne(User::class, 'nik','nik');
    }

    public function permission(): HasOne
    {
        return $this->hasOne(Permission::class, 'id','id_perusahaan');
    }
}
