<?php

use App\Http\Controllers\Perusahaan\CatatanController;
use App\Http\Controllers\Perusahaan\PencarianController;
use App\Http\Controllers\PengalamanController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PerimissionController;
use App\Http\Controllers\Perusahaan\ProfileController as PerusahaanProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::middleware('guest')->group(function () {
    Route::get('/', function () {
        return Inertia::render('Welcome', [
            'canLogin' => Route::has('login'),
            'canRegister' => Route::has('register'),
            'laravelVersion' => Application::VERSION,
            'phpVersion' => PHP_VERSION,
        ]);
    })->name('welcome');
});

Route::get('/welcome', function () {
    return Inertia::render('Welcome');
})->middleware(['auth', 'verified'])->name('welcome');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::post('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::post('/pengalaman', [PengalamanController::class, 'store'])->name('pengalaman.store');
    Route::patch('/pengalaman', [PengalamanController::class, 'update'])->name('pengalaman.update');
    Route::delete('/pengalaman', [PengalamanController::class, 'destroy'])->name('pengalaman.destroy');

    Route::post('/permission', [PerimissionController::class, 'store'])->name('permission.store');
    Route::patch('/permission', [PerimissionController::class, 'update'])->name('permission.update');
    Route::delete('/permission', [PerimissionController::class, 'destroy'])->name('permission.destroy');

    Route::group(['prefix' => 'perusahaan'], function () {
        Route::get('/profile', [PerusahaanProfileController::class, 'index'])->name('perusahaan.profile.index');
        Route::post('/profile', [PerusahaanProfileController::class, 'update'])->name('perusahaan.profile.update');

        Route::get('/pencarian', [PencarianController::class, 'index'])->name('pencarian.index');

        Route::post('/catatan', [CatatanController::class, 'store'])->name('catatan.store');
    });
});

require __DIR__.'/auth.php';
