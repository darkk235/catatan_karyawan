<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tb_pengalaman', function (Blueprint $table) {
            $table->id('id');
            $table->string('nik');
            $table->string('perusahaan');
            $table->string('jabatan');
            $table->string('mulai_bekerja');
            $table->string('selesai_bekerja');
            $table->string('masih_aktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tb_pengalaman');
    }
};
