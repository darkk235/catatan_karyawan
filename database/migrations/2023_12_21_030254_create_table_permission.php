<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tb_permission', function (Blueprint $table) {
            $table->id('id_permission');
            $table->integer('id_perusahaan');
            $table->string('nik');
            $table->string('akses_edit');
            $table->string('akses_view');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tb_permission');
    }
};
