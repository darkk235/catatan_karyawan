import { Link, Head, usePage } from '@inertiajs/react';

const GuestLayout = ({ children }) => {
    const { auth,ziggy,hasil } = usePage().props;
    return (
        <section className="user-login section">
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="block">
                            <div className="image align-self-center">
                                <img className="img-fluid" src={`${ziggy.url}/images/Login/front-desk-sign-in.jpg`} alt="desk-image" />
                            </div>
                            <div className="content text-center">
                                <div className="logo">
                                    <a href="/"><img src={`${ziggy.url}/images/logo.png`} alt="logo" /></a>
                                </div>
                
                                { children }
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default GuestLayout;
