import { usePage } from "@inertiajs/react";
import { Link, Head } from '@inertiajs/react';

const KaryawanLayout = ({ children }) => {
    const { auth,ziggy } = usePage().props;

    return (
        <>
            <nav className="navbar main-nav navbar-expand-lg px-2 px-sm-0 py-2 py-lg-0">
                <div className="container">
                <a className="navbar-brand" href="#"><img src={`${ziggy.url}/images/logo.png`} alt="logo" /></a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="ti-menu"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item ">
                                <Link className='nav-link' href="/">Home</Link>
                            </li>

                            {auth.user ?
                                <li className="nav-item @@contact" >
                                    <Link href={route('logout')} className='nav-link'>Logout</Link>
                                </li>
                                :
                                <>
                                    <li className="nav-item @@contact" >
                                        <Link className='nav-link' href={route('login')}>Login</Link>
                                    </li>
                                    <li className="nav-item @@contact">
                                        <Link className='nav-link' href={route('register')}>Daftar</Link>
                                    </li>
                                </>
                            }

                        </ul>
                    </div>
                </div>
            </nav>
            <section className="section page-title">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-8 m-auto">
                            <h1>Selamat Datang</h1>
                            <p>Di catatan karyawan</p>
                        </div>
                    </div>
                </div>
            </section>

            <section className="privacy section pt-0">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3">
                            <nav className="privacy-nav">
                                <ul>
                                    <li><a className="nav-link scrollTo" href="#profile">Profile</a></li>
                                    <li><a className="nav-link scrollTo" href="#pengalaman">Pengalaman Kerja</a></li>

                                    <li><a className="nav-link scrollTo" href="#catatan">Catatan</a></li>
                                    <li><a className="nav-link scrollTo" href="#approval">Approval</a></li>
                                </ul>
                            </nav>
                        </div>

                        {children}

                    </div>
                </div>
            </section>

            <footer>
                <div className="footer-main">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-4 col-md-12 m-md-auto align-self-center">
                                <div className="block">
                                    <a href="#"><img src={`${ziggy.url}/images/logo-alt.png`} alt="footer-logo" /></a>
                                    <ul className="social-icon list-inline">
                                        <li className="list-inline-item">
                                            <a href="https://www.facebook.com/themefisher"><i className="ti-facebook"></i></a>
                                        </li>
                                        <li className="list-inline-item">
                                            <a href="https://twitter.com/themefisher"><i className="ti-twitter"></i></a>
                                        </li>
                                        <li className="list-inline-item">
                                            <a href="https://www.instagram.com/themefisher/"><i className="ti-instagram"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-3 col-6 mt-5 mt-lg-0">
                                <div className="block-2">
                                    <h6>About Me</h6>
                                    <ul>
                                        <li><a href="FAQ.html">About</a></li>
                                        <li><a href="team.html">Teams</a></li>
                                        <li><a href="blog.html">Contact</a></li>

                                    </ul>
                                </div>
                            </div>

                            <div className="col-lg-2 col-md-3 col-6 mt-5 mt-lg-0">
                                <div className="block-2">
                                    <h6>Company</h6>
                                    <ul>
                                        <li><a href="career.html">Career</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                        <li><a href="team.html">Investor</a></li>
                                        <li><a href="privacy.html">Terms</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-3 col-6 mt-5 mt-lg-0">
                                <div className="block-2">
                                    <h6>Company</h6>
                                    <ul>
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                        <li><a href="team.html">Team</a></li>
                                        <li><a href="privacy-policy.html">Privacy Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="text-center bg-blue py-4">
                    <small className="text-white">Copyright &copy; <script>document.write(new Date().getFullYear())</script>. Designed &amp; Developed by <a href="#">Catatan Karyawan</a></small>
                </div>
            </footer>

            <div className="scroll-top-to">
                <i className="ti-angle-up"></i>
            </div>
        </>
    );
};

export default KaryawanLayout;
