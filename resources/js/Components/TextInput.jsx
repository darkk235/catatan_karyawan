import { forwardRef, useEffect, useRef } from 'react';

export default forwardRef(function TextInput({ value,type = 'text', className = '', isFocused = false, onChange,...props }, ref) {
    const input = ref ? ref : useRef();

    useEffect(() => {
        if (isFocused) {
            input.current.focus();
        }
    }, []);

    return (
        <input {...props} type={type} defaultValue={value} className={'form-control main' + className} onChange={onChange} ref={input}/>
    );
});
