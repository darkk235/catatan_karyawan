export default function PrimaryButton({ className = '', disabled, label,type= "submit", ...props }) {
    return (
        <button {...props} type={type} className={'btn-sm btn btn-primary' + className} disabled={disabled}>
            {label}
        </button>
    );
}
