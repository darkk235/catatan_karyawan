import React from "react";
import { Link } from "@inertiajs/react";

const NavLink = ({badges, label, href, active = false, iconName }) => {
    return (
        <li className="nav-item">
            <Link href={href} className={`nav-link ${active ? "active" : ""}`}>
                <i className={`nav-icon ${iconName}`} />
                <p>{label} <span className="badge badge-danger right">{badges ? badges : ""}</span></p>
            </Link>
        </li>
    );
};

export default NavLink;
