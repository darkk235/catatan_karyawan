import { forwardRef, useEffect, useRef } from 'react';

export default forwardRef(function TextInput({ children,onChange,defaultValue, className = '', isFocused = false, ...props }, ref) {
    const input = ref ? ref : useRef();

    useEffect(() => {
        if (isFocused) {
            input.current.focus();
        }
    }, []);

    return (
        <select {...props} className={'form-control main' + className} defaultValue={defaultValue} onChange={onChange} ref={input}>
            {children}
        </select>
    );
});
