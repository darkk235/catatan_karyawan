export default function DangerButton({ className = '', disabled, label,type= "submit", ...props }) {
    return (
        <button {...props} type={type} className={'btn-sm btn btn-danger' + className} disabled={disabled}>
            {label}
        </button>
    );
}
