import { Head, useForm, usePage } from '@inertiajs/react';
import GuestLayout from '@/Layouts/GuestLayout';
import InputLabel from '@/Components/InputLabel';
import TextInput from '@/Components/TextInput';
import InputError from '@/Components/InputError';
import PrimaryButton from '@/Components/PrimaryButton';
import ModalStatic from '@/Components/ModalStatic';
import React from 'react';

export default function UpdateCatatan({ datas, onShow, onClose }) {
    const { auth } = usePage().props
    const [formAdd, setFormAdd] = React.useState(false)

    const { setData, data, processing, progress, post, errors } = useForm({
        id_catatan: "",
        catatan: "",
        nik:datas.nik
    });

    const handleCatatan = (value, ids,niks) => {
        setData({
            catatan: value,
            id_catatan: ids,
            nik:datas.nik
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        post(route('catatan.store'), {
            onSuccess: () => onClose(),
            onError: (text) => console.log(text)
        });
    };

    return (
        <ModalStatic show={onShow} handleClose={onClose} onSubmit={handleSubmit} title="Data Karyawan">
            <div className="row">
                <div className="p-3 ">
                    <div className="d-flex flex-row justify-content-center mb-3">
                        <div className="image"> <img src={auth.user.avatar ?? 'http://127.0.0.1:8000/images/beruang.png'} className="rounded-circle" width={200} height={200} /> <span><i className='bx bxs-camera-plus'></i></span> </div>
                    </div>
                    <div className="d-flex text-center flex-column  justify-content-center ">
                        <h4 className="mb-0">{datas.name}</h4>
                        <span>{datas.jabatan}</span>
                    </div>
                    <br />
                    <div className="card mb-3">
                        <div className="card-header"><h3>Profile</h3></div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col-md-6">
                                    <InputLabel value="Email" />
                                    <TextInput name="email" disabled value={datas.email} onChange={(e) => setData('email', e.target.value)} />
                                </div>
                                <div className="col-md-6">
                                    <InputLabel value="No Handphone" />
                                    <TextInput name="no_hp" disabled value={datas.no_hp} onChange={(e) => setData('no_hp', e.target.value)} />
                                </div>
                                <div className="col-md-6">
                                    <InputLabel value="Username" />
                                    <TextInput name="username" disabled value={datas.username} onChange={(e) => setData('username', e.target.value)} />
                                </div>
                                <div className="col-md-6">
                                    <InputLabel value="NIK" />
                                    <TextInput name="nik" disabled value={datas.nik} onChange={(e) => setData('nik', e.target.value)} />
                                </div>

                                <div className="col-md-12">
                                    <div className="about-inputs mb-3">
                                        <label>Deskripsi</label>
                                        <textarea disabled name="deskripsi" className="form-control" type="text" onChange={(e) => setData('deskripsi', e.target.value)} defaultValue={data.deskripsi} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            <h3 className="float-left">Catatan</h3>
                            <button className='float-right btn-sm btn btn-primary' type='button' onClick={() => setFormAdd(true)}>Tambah</button>
                        </div>
                        <div className="card-body">
                            {formAdd &&
                                <div className="row mb-3">
                                    <div className="col-md-12">
                                        <div className="about-inputs">
                                            <label>{auth.user.name}</label>
                                            <InputError message={errors.deskripsi} />
                                            <textarea name="deskripsi" className="form-control" type="text" onChange={(e) => setData('catatan', e.target.value)} defaultValue={data.deskripsi} />
                                        </div>
                                    </div>
                                </div>
                            }

                            {datas.catatan.map((val, key) => {
                                return (
                                    val.permission ? val.permission.akses_view == 'ya' &&
                                    <div key={key} className="mb-3">
                                        <div className="about-inputs">
                                            <label>{val.perusahaan.name}</label>
                                            <textarea disabled={val.permission && val.permission.akses_edit == 'ya' && auth.user.id == val.id_perusahaan ? false : true} name="catatan" className="form-control" type="text" onChange={(e) => handleCatatan(e.target.value, val.id)} defaultValue={val.catatan} />
                                        </div>
                                    </div>
                                    :
                                    <div key={key} className="mb-3">
                                        <div className="about-inputs">
                                            <label>{val.perusahaan.name}</label>
                                            <textarea disabled={val.permission && val.permission.akses_edit == 'ya' && auth.user.id == val.id_perusahaan ? false : true} name="catatan" className="form-control" type="text" onChange={(e) => handleCatatan(e.target.value, val.id)} defaultValue={val.catatan} />
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </ModalStatic>
    );
}
