import PerusahaanLayout from "@/Layouts/PerusahaanLayout";
import React from 'react';
import DataTable from 'react-data-table-component';
import UpdateProfileInformation from './Partials/UpdateProfileInformationForm';

const Index = ({catatan}) => {
    const [modalShow, setModalShow] = React.useState({
        editCatatan: false,
        hapusCatatan: false,
        data: []
    });
    return (
        <PerusahaanLayout>
            <div className="col-lg-9">
                <div className="block">
                    <UpdateProfileInformation />

                    <div id="catatan" className="policy-item">
                        <div className="title ">
                            <h3 className="float-left">Riwayat Catatan</h3>
                        </div>
                        <DataTable
                            className="table table-sm table-bordered"
                            data={catatan}
                            pagination
                            noHeader
                            highlightOnHover
                            pointerOnHover
                            columns={[
                                {
                                    name: 'Nama Karyawan',
                                    selector: row => row.user_nik.name,
                                    sortable: true,
                                },
                                {
                                    name: 'Catatan',
                                    selector: row => row.catatan,
                                    sortable: true,
                                },
                            ]}
                        />
                    </div>
                </div>
            </div>
        </PerusahaanLayout>
    )
}

export default Index;