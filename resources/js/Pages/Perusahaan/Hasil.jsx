import DangerButton from "@/Components/DangerButton";
import PrimaryButton from "@/Components/PrimaryButton";
import PerusahaanLayout from "@/Layouts/PerusahaanLayout";
import React from 'react';
import DataTable from 'react-data-table-component';
import UpdateCatatan from "./Partials/UpdateCatatan";
import { Link, useForm } from '@inertiajs/react';
import InputError from '@/Components/InputError';
import TextInput from '@/Components/TextInput';
import FormSelect from '@/Components/FormSelect';
import InputLabel from '@/Components/InputLabel';

const Hasil = ({ hasil }) => {
    const [modalShow, setModalShow] = React.useState({
        editCatatan: false,
        hapusCatatan: false,
        data: []
    });

    const handleOpenModal = (e, row, aksi) => {
        e.preventDefault();
        setModalShow({ data: row, [aksi]: true });
    };

    const handleCloseModal = (row, aksi) => {
        setModalShow({ data: row, [aksi]: false });
    };

    const { data, setData, get, processing, errors, reset } = useForm({
        pencarian: "",
        kategori: ""
    });

    const handleSubmit = (e) => {
        e.preventDefault();

        get(route('pencarian.index'), {
            preserveState: false,
            replace: true
        });
    };

    return (
        <PerusahaanLayout>
            <div className="col-lg-9">
                <div className="card mb-4">
                    <div className="card-header">Pencarian</div>
                    <div className="card-body">
                    <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <InputLabel value="Kata Kunci" />
                        <InputError message={errors.pencarian} />
                        <TextInput type="text" name="pencarian" value={data.pencarian} autoComplete="pencarian" placeholder="Pencarian" isFocused={true} onChange={(e) => setData('pencarian', e.target.value)} />
                    </div>

                    <div className="form-group">
                        <InputLabel value="Cari Berdasarkan" />
                        <InputError message={errors.masih_aktif} />
                        <FormSelect name="pencarian" defaultValue={data.kategori} onChange={(e) => setData('kategori', e.target.value)}>
                            <option value="">--- Pilih Kategori ---</option>
                            <option value="Perusahaan">Perusahaan</option>
                            <option value="Karyawan">Calon Karyawan</option>
                            <option value="Jabatan">Bidang Kerja</option>
                        </FormSelect>
                    </div>

                    <PrimaryButton type="submit" label={'Cari'}/>
                </form>
                    </div>
                </div>
                <div className="block">
                    <div className="policy-item">
                        <div className="title ">
                            <h3 className="float-left">Hasil Pencarian</h3>
                        </div>
                        <DataTable
                            className="table table-sm table-bordered"
                            data={hasil}
                            pagination
                            noHeader
                            highlightOnHover
                            pointerOnHover
                            columns={[
                                {
                                    name: 'No Handphone',
                                    selector: row => row.no_hp,
                                    sortable: true,
                                },
                                {
                                    name: 'Nama Karyawan',
                                    selector: row => row.name,
                                    sortable: true,
                                },
                                {
                                    name: 'Jabatan',
                                    selector: row => row.jabatan,
                                    sortable: true,
                                },
                                {
                                    name: 'NIK',
                                    selector: row => row.nik,
                                    sortable: true,
                                },
                                {
                                    name: "Aksi",
                                    selector: (row) => row.id,
                                    cell: (row) => (
                                        <>
                                            <PrimaryButton label="Lihat" onClick={(e) => handleOpenModal(e, row, "editCatatan")} />
                                        </>
                                    ),
                                },
                            ]}
                        />
                    </div>
                </div>
            </div>

            {modalShow.editCatatan &&
                <UpdateCatatan datas={modalShow.data} onShow={modalShow.editCatatan} onClose={() => handleCloseModal([], 'editCatatan')} />
            }
        </PerusahaanLayout>
    )
}

export default Hasil;