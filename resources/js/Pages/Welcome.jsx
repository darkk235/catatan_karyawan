import GuestLayout from '@/Layouts/GuestLayout';
import { Link, Head } from '@inertiajs/react';

export default function Welcome({ auth, laravelVersion, phpVersion }) {
    return (
        <>
            <nav className="navbar main-nav navbar-expand-lg px-2 px-sm-0 py-2 py-lg-0">
                <div className="container">
                    <a className="navbar-brand" href="index.html"><img src="images/logo.png" alt="logo" /></a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="ti-menu"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item ">
                                <Link className='nav-link' href="/">Home</Link>
                            </li>

                            {auth.user ?
                                <>
                                    <li className="nav-item @@contact" >
                                        <Link className='nav-link' href={auth.user.role== 'Perusahaan' ? route('perusahaan.profile.index') : route('profile.edit')}>Profile</Link>
                                    </li>
                                    <li className="nav-item @@contact" >
                                        <Link href={route('logout')} className='nav-link'>Logout</Link>
                                    </li>
                                </>
                                :
                                <>
                                    <li className="nav-item @@contact" >
                                        <Link className='nav-link' href={route('login')}>Login</Link>
                                    </li>
                                    <li className="nav-item @@contact">
                                        <Link className='nav-link' href={route('register')}>Daftar</Link>
                                    </li>
                                </>
                            }

                        </ul>
                    </div>
                </div>
            </nav>
            <section className="section gradient-banner">
                <div className="shapes-container">
                    <div className="shape" data-aos="fade-down-left" data-aos-duration="1500" data-aos-delay="100"></div>
                    <div className="shape" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="100"></div>
                    <div className="shape" data-aos="fade-up-right" data-aos-duration="1000" data-aos-delay="200"></div>
                    <div className="shape" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200"></div>
                    <div className="shape" data-aos="fade-down-left" data-aos-duration="1000" data-aos-delay="100"></div>
                    <div className="shape" data-aos="fade-down-left" data-aos-duration="1000" data-aos-delay="100"></div>
                    <div className="shape" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300"></div>
                    <div className="shape" data-aos="fade-down-right" data-aos-duration="500" data-aos-delay="200"></div>
                    <div className="shape" data-aos="fade-down-right" data-aos-duration="500" data-aos-delay="100"></div>
                    <div className="shape" data-aos="zoom-out" data-aos-duration="2000" data-aos-delay="500"></div>
                    <div className="shape" data-aos="fade-up-right" data-aos-duration="500" data-aos-delay="200"></div>
                    <div className="shape" data-aos="fade-down-left" data-aos-duration="500" data-aos-delay="100"></div>
                    <div className="shape" data-aos="fade-up" data-aos-duration="500" data-aos-delay="0"></div>
                    <div className="shape" data-aos="fade-down" data-aos-duration="500" data-aos-delay="0"></div>
                    <div className="shape" data-aos="fade-up-right" data-aos-duration="500" data-aos-delay="100"></div>
                    <div className="shape" data-aos="fade-down-left" data-aos-duration="500" data-aos-delay="0"></div>
                </div>
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-md-6 order-2 order-md-1 text-center text-md-left">
                            <h1 className="text-white font-weight-bold mb-4">Lihat Catatan Pegawai </h1>
                            <p className="text-white mb-5">Mau Lihat Catatan Pegawai / perusahaan ?</p>
                        </div>
                        <div className="col-md-6 text-center order-1 order-md-2">
                            <img className="img-fluid" src="images/beruang.png" alt="screenshot" />
                        </div>
                    </div>
                </div>
            </section>

            <section className="section pt-0 position-relative pull-top">
                <div className="container">
                    <div className="rounded shadow p-5 bg-white">
                        <div className="row">
                            <div className="col-lg-4 col-md-6 mt-5 mt-md-0 text-center">
                                <i className="ti-paint-bucket text-primary h1"></i>
                                <h3 className="mt-4 text-capitalize h5 ">Tentang Kami</h3>
                                <p className="regular text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam non, recusandae
                                    tempore ipsam dignissimos molestias.</p>
                            </div>
                            <div className="col-lg-4 col-md-6 mt-5 mt-md-0 text-center">
                                <i className="ti-shine text-primary h1"></i>
                                <h3 className="mt-4 text-capitalize h5 ">Kontak</h3>
                                <p className="regular text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam non, recusandae
                                    tempore ipsam dignissimos molestias.</p>
                            </div>
                            <div className="col-lg-4 col-md-12 mt-5 mt-lg-0 text-center">
                                <i className="ti-thought text-primary h1"></i>
                                <h3 className="mt-4 text-capitalize h5 ">Syarat Ketentuan</h3>
                                <p className="regular text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquam non, recusandae tempore ipsam dignissimos molestias.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}
