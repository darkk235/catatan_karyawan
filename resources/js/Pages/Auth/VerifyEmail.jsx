import GuestLayout from '@/Layouts/GuestLayout';
import PrimaryButton from '@/Components/PrimaryButton';
import { Head, Link, useForm } from '@inertiajs/react';

export default function VerifyEmail({ status }) {
    const { post, processing } = useForm({});

    const submit = (e) => {
        e.preventDefault();

        post(route('verification.send'));
    };

    return (
        <GuestLayout>
            <div className="card">
                <div className="card-header costumColor h2 text-center"><strong>Email Verification</strong></div>
                <div className="card-body">
                    <div className="card mb-4">
                        <div className="card-body">
                            Thanks for signing up! Before getting started, could you verify your email address by clicking on the
                            link we just emailed to you? If you didn't receive the email, we will gladly send you another.
                        </div>
                    </div>

                    {status === 'verification-link-sent' && (
                        <div className="card mb-4">
                            <div className="card-body">
                                A new verification link has been sent to the email address you provided during registration.
                            </div>
                        </div>
                    )}
                </div>
            </div>

            <form onSubmit={submit}>
                <div className="mt-4 flex items-center justify-between">
                    <PrimaryButton label="Resend Verification Email" disabled={processing}/>
                </div>
            </form>
        </GuestLayout>
    );
}
