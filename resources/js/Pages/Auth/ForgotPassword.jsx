import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, Link, useForm } from '@inertiajs/react';
import InputLabel from '@/Components/InputLabel';

export default function ForgotPassword({ status }) {
    const { data, setData, post, processing, errors } = useForm({
        email: '',
    });

    const handleSubmit = (e) => {
        e.preventDefault();

        post(route('password.email'));
    };

    return (
        <GuestLayout>
            <form onSubmit={handleSubmit}>
                <div className="title-text">
                    <h3>Verifikasi Email</h3>
                    Lupa kata sandi Anda? Tidak masalah. Cukup beri tahu kami alamat email Anda dan kami akan mengirimkan email berisi tautan pengaturan ulang kata sandi yang memungkinkan Anda memilih yang baru.
                </div>

                <div className="form-group">
                    <InputLabel value="Email" />
                    <InputError message={errors.username} />
                    <TextInput type="email" name="email" placeholder="email" value={data.username} autoComplete="email" isFocused={true} onChange={(e) => setData('email', e.target.value)} />
                </div>

                <PrimaryButton label="Send" />

                <div className="new-acount">
                    <Link href={route('login')} className='text-muted'><strong>Kembali</strong></Link>
                </div>
            </form>
        </GuestLayout>
    );
}
