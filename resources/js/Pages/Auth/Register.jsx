import { useEffect } from 'react';
import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, Link, useForm } from '@inertiajs/react';
import FormSelect from '@/Components/FormSelect';

export default function Register() {
    const { data, setData, post, processing, errors, reset } = useForm({
        role: '',
        name: '',
        email: '',
        username: '',
        nik: '',
        no_hp: '',
        password: '',
        password_confirmation: '',
    });

    useEffect(() => {
        return () => {
            reset('password', 'password_confirmation');
        };
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();

        post(route('register'));
    };

    return (
        <GuestLayout>
            <form onSubmit={handleSubmit}>
                <div className="title-text">
                    <h3>Silahkan Daftar</h3>
                </div>
                <InputError message={errors.role} />
                <FormSelect name="role" placeholder="Daftar Sebagai" onChange={(e) => setData('role', e.target.value)}>
                    <option>--Daftar Sebagai--</option>
                    <option value="Perusahaan">Perusahan</option>
                    <option value="Karyawan">Karyawan</option>
                </FormSelect>

                <InputError message={errors.name} />
                <TextInput type="text" name="name" placeholder="Nama" value={data.name} autoComplete="name" isFocused={true} onChange={(e) => setData('name', e.target.value)} />

                <InputError message={errors.email} />
                <TextInput type="email" name="email" placeholder="Email" value={data.email} autoComplete="email" isFocused={true} onChange={(e) => setData('email', e.target.value)} />

                <InputError message={errors.username} />
                <TextInput type="text" name="username" placeholder="Username" value={data.username} autoComplete="username" isFocused={true} onChange={(e) => setData('username', e.target.value)} />

                <InputError message={errors.nik} />
                <TextInput type="number" name="nik" placeholder="NIK" value={data.nik} autoComplete="nik" isFocused={true} onChange={(e) => setData('nik', e.target.value)} />

                <InputError message={errors.no_hp} />
                <TextInput type="number" name="no_hp" placeholder="No Handphone" value={data.no_hp} autoComplete="no_hp" isFocused={true} onChange={(e) => setData('no_hp', e.target.value)} />

                <InputError message={errors.password} />
                <TextInput type="password" name="password" placeholder="Password" value={data.password} autoComplete="new-password" isFocused={true} onChange={(e) => setData('password', e.target.value)} />

                <InputError message={errors.password_confirmation} />
                <TextInput type="password" name="password_confirmation" placeholder="Konfirmasi Password" value={data.password_confirmation} autoComplete="new-password" isFocused={true} onChange={(e) => setData('password_confirmation', e.target.value)} />

                <PrimaryButton label="Daftar" />

                <div className="new-acount">
                    <p>Apakah anda mempunyai akun ?  <Link href={route('login')} className='text-muted'><strong>Login</strong></Link></p>
                </div>
            </form>
        </GuestLayout>
    );
}
