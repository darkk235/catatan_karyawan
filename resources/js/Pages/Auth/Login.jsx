import { useEffect } from 'react';
import Checkbox from '@/Components/Checkbox';
import GuestLayout from '@/Layouts/GuestLayout';
import InputError from '@/Components/InputError';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { Head, Link, useForm } from '@inertiajs/react';

export default function Login({ status, canResetPassword }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        username: '',
        password: '',
        remember: false,
    });

    useEffect(() => {
        return () => {
            reset('password');
        };
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();

        post(route('login'));
    };

    return (
        <GuestLayout>
            <form onSubmit={handleSubmit}>
                <div className="title-text">
                    <h3>Silahkan Login</h3>
                </div>
                
                <InputError message={errors.username} />
                <TextInput type="text" name="username" placeholder="Username" value={data.username} autoComplete="username" isFocused={true} onChange={(e) => setData('username', e.target.value)} />

                <InputError message={errors.password} />
                <TextInput type="password" name="password" placeholder="Password" value={data.password} autoComplete="new-password" isFocused={true} onChange={(e) => setData('password', e.target.value)} />

                <PrimaryButton label="Login" />

                <div className="new-acount">
                    <Link href={route('password.request')} className='text-muted'><strong>Lupa Password ?</strong></Link>
                    <p>Apakah anda belum mempunyai akun ?  <Link href={route('register')} className='text-muted'><strong>Daftar</strong></Link></p>
                </div>
            </form>
        </GuestLayout>
    );
}
