import { useEffect } from 'react';
import InputError from '@/Components/InputError';
import InputLabel from '@/Components/InputLabel';
import PrimaryButton from '@/Components/PrimaryButton';
import TextInput from '@/Components/TextInput';
import { useForm } from '@inertiajs/react';
import GuestLayout from '@/Layouts/GuestLayout';

export default function ResetPassword({ token, email }) {
    const { data, setData, post, processing, errors, reset } = useForm({
        token: token,
        email: email,
        password: '',
        password_confirmation: '',
    });

    useEffect(() => {
        return () => {
            reset('password', 'password_confirmation');
        };
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();

        post(route('password.store'));
    };

    return (
        <GuestLayout>
            <form onSubmit={handleSubmit}>
            <InputError message={errors.password} />
                <TextInput type="password" name="password" placeholder="Password" value={data.password} autoComplete="new-password" isFocused={true} onChange={(e) => setData('password', e.target.value)} />

                <InputError message={errors.password_confirmation} />
                <TextInput type="password" name="password_confirmation" placeholder="Konfirmasi Password" value={data.password_confirmation} autoComplete="new-password" isFocused={true} onChange={(e) => setData('password_confirmation', e.target.value)} />


                <PrimaryButton label="Reset Password" />
            </form>
        </GuestLayout>
    );
}
