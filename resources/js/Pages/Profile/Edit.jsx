import React from 'react';
import DataTable from 'react-data-table-component';
import UpdateProfileInformation from './Partials/UpdateProfileInformationForm';
import UpdatePermission from './Partials/UpdatePermission';
import StorePengalaman from './Partials/StorePengalaman';
import PrimaryButton from '@/Components/PrimaryButton';
import UpdatePengalaman from './Partials/UpdatePengalaman';
import DangerButton from '@/Components/DangerButton';
import DeletePengalaman from './Partials/DeletePengalaman';
import StorePermission from './Partials/StorePermission';
import DeletePermission from './Partials/DeletePermission';
import KaryawanLayout from '@/Layouts/KaryawanLayout';

export default function Edit({ pengalaman, permission, catatan }) {
    const [modalShow, setModalShow] = React.useState({
        editPengalaman: false,
        hapusPengalaman: false,
        editPermission: false,
        hapusPermission: false,
        data: []
    });

    const handleOpenModal = (e, row, aksi) => {
        e.preventDefault();
        setModalShow({ data: row, [aksi]: true });
    };

    const handleCloseModal = (row, aksi) => {
        setModalShow({ data: row, [aksi]: false });
    };

    return (
        <KaryawanLayout>
            <div className="col-lg-9">
                <div className="block">

                    <UpdateProfileInformation />

                    <div id="pengalaman" className="policy-item">

                        <StorePengalaman />

                        <DataTable
                            className="table table-sm table-bordered"
                            data={pengalaman}
                            pagination
                            noHeader
                            highlightOnHover
                            pointerOnHover
                            columns={[
                                {
                                    name: 'Perusahaan',
                                    selector: row => row.perusahaan,
                                    sortable: true,
                                },
                                {
                                    name: 'Jabatan',
                                    selector: row => row.jabatan,
                                    sortable: true,
                                },
                                {
                                    name: 'Mulai Bekerja',
                                    selector: row => row.mulai_bekerja,
                                    sortable: true,
                                },
                                {
                                    name: 'Selesai Bekerja',
                                    selector: row => row.selesai_bekerja,
                                    sortable: true,
                                },
                                {
                                    name: "Aksi",
                                    selector: (row) => row.id_pengalaman,
                                    cell: (row) => (
                                        <>
                                            <PrimaryButton label="Edit" onClick={(e) => handleOpenModal(e, row, "editPengalaman")} />
                                            <DangerButton label="Hapus" onClick={(e) => handleOpenModal(e, row, "hapusPengalaman")} />
                                        </>
                                    ),
                                },
                            ]}
                        />

                        {modalShow.editPengalaman &&
                            <UpdatePengalaman datas={modalShow.data} onShow={modalShow.editPengalaman} onClose={() => handleCloseModal([], 'editPengalaman')} />
                        }

                        {modalShow.hapusPengalaman &&
                            <DeletePengalaman datas={modalShow.data} onShow={modalShow.hapusPengalaman} onClose={() => handleCloseModal([], 'hapusPengalaman')} />
                        }
                    </div>

                    <div id="catatan" className="policy-item">
                        <div className="title ">
                            <h3 className="float-left">Catatan</h3>
                        </div>
                        <DataTable
                            className="table table-sm table-bordered"
                            data={catatan}
                            pagination
                            noHeader
                            highlightOnHover
                            pointerOnHover
                            columns={[
                                {
                                    name: 'Perusahaan',
                                    selector: row => row.perusahaan.name,
                                    sortable: true,
                                },
                                {
                                    name: 'Catatan',
                                    selector: row => row.catatan,
                                    sortable: true,
                                },
                            ]}
                        />
                    </div>

                    <div id="approval" className="policy-item">
                        <StorePermission />

                        <DataTable
                            className="table table-sm table-bordered"
                            data={permission}
                            pagination
                            noHeader
                            highlightOnHover
                            pointerOnHover
                            columns={[
                                {
                                    name: 'Perusahaan',
                                    selector: row => row.perusahaan.name,
                                    sortable: true,
                                },
                                {
                                    name: "Aksi",
                                    selector: (row) => row.id,
                                    cell: (row) => (
                                        <>
                                            <PrimaryButton label="Edit" onClick={(e) => handleOpenModal(e, row, "editPermission")} />
                                            <DangerButton label="Hapus" onClick={(e) => handleOpenModal(e, row, "hapusPermission")} />
                                        </>
                                    ),
                                },
                            ]}
                        />

                        {modalShow.editPermission &&
                            <UpdatePermission datas={modalShow.data} onShow={modalShow.editPermission} onClose={() => handleCloseModal([], 'editPermission')} />
                        }

                        {modalShow.hapusPermission &&
                            <DeletePermission datas={modalShow.data} onShow={modalShow.hapusPermission} onClose={() => handleCloseModal([], 'hapusPermission')} />
                        }
                    </div>
                </div>
            </div>
        </KaryawanLayout>
    );
}
