import React from 'react';
import { Head, useForm, usePage } from '@inertiajs/react';
import GuestLayout from '@/Layouts/GuestLayout';
import InputLabel from '@/Components/InputLabel';
import TextInput from '@/Components/TextInput';
import InputError from '@/Components/InputError';
import PrimaryButton from '@/Components/PrimaryButton';
import ModalStatic from '@/Components/ModalStatic';
import FormSelect from '@/Components/FormSelect';

export default function StorePengalaman({onClose}) {
    const { auth } = usePage().props
    const [show, setShow] = React.useState(false);

    const { setData, data, post, processing, errors,reset } = useForm({
        nik: auth.user.nik,
        perusahaan: "",
        jabatan: "",
        mulai_bekerja: "",
        selesai_bekerja: "",
        masih_aktif: "aktif",
    });

    const handleStore = (e) => {
        e.preventDefault();

        post(route('pengalaman.store'),{
            onSuccess: () => {
                reset(),
                handleClose()
            },
            onError:(text) => console.log(text),
        });
    };

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <div className="title ">
                <h3 className="float-left">Pengalaman Kerja</h3>
                <button className='float-right btn-sm btn btn-primary' label="primary" onClick={handleShow}>Tambah</button>
            </div>

            <ModalStatic show={show} handleClose={handleClose} onSubmit={handleStore} title="Tambah Pengalaman">
                <div className="row">
                    <div className="col">
                        <div className="form-group">
                            <InputLabel value="NIK" />
                            <InputError message={errors.nik} />
                            <TextInput name="nik" value={data.nik} onChange={(e) => setData('nik', e.target.value)} />
                        </div>
                        <div className="form-group">
                            <InputLabel value="Perusahaan" />
                            <InputError message={errors.perusahaan} />
                            <TextInput name="perusahaan" value={data.perusahaan} onChange={(e) => setData('perusahaan', e.target.value)} />
                        </div>
                        <div className="form-group">
                            <InputLabel value="Jabatan" />
                            <InputError message={errors.jabatan} />
                            <TextInput name="jabatan" value={data.jabatan} onChange={(e) => setData('jabatan', e.target.value)} />
                        </div>
                        <div className="form-group">
                            <InputLabel value="Mulai Bekerja" />
                            <InputError message={errors.mulai_bekerja} />
                            <TextInput type="date" name="mulai_bekerja" value={data.mulai_bekerja} onChange={(e) => setData('mulai_bekerja', e.target.value)} />
                        </div>
                        <div className="form-group">
                            <InputLabel value="Selesai Bekerja" />
                            <InputError message={errors.selesai_bekerja} />
                            <TextInput type="date" name="selesai_bekerja" value={data.selesai_bekerja} onChange={(e) => setData('selesai_bekerja', e.target.value)} />
                        </div>
                        <div className="form-group">
                            <InputLabel value="Masih Aktif" />
                            <InputError message={errors.masih_aktif} />
                            <FormSelect name="masih_aktif" onChange={(e) => setData('masih_aktif', e.target.value)}>
                                <option value="aktif">Ya</option>
                                <option value="tidak">Tidak</option>
                            </FormSelect>
                        </div>
                    </div>
                </div>
            </ModalStatic>
        </>
    );
}
