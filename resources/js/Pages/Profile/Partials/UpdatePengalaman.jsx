import React from 'react';
import { Head, useForm, usePage } from '@inertiajs/react';
import InputLabel from '@/Components/InputLabel';
import TextInput from '@/Components/TextInput';
import InputError from '@/Components/InputError';
import ModalStatic from '@/Components/ModalStatic';
import FormSelect from '@/Components/FormSelect';

export default function UpdatePengalaman({datas,onShow,onClose}) {
    const { setData, data, patch, processing, errors } = useForm(datas);

    const handleStore = (e) => {
        e.preventDefault();

        patch(route('pengalaman.update'),{
            onSuccess: () => onClose(),
            onError:(text) => console.log(text)
        });
    };

    return (
        <ModalStatic show={onShow} handleClose={onClose} onSubmit={handleStore} title="Update Pengalaman">
            <div className="row">
                <div className="col">
                    <div className="form-group">
                        <InputLabel value="NIK" />
                        <InputError message={errors.nik} />
                        <TextInput disabled name="nik" value={datas.nik} onChange={(e) => setData('nik', e.target.value)} />
                    </div>
                    <div className="form-group">
                        <InputLabel value="Perusahaan" />
                        <InputError message={errors.perusahaan} />
                        <TextInput name="perusahaan" value={datas.perusahaan} onChange={(e) => setData('perusahaan', e.target.value)} />
                    </div>
                    <div className="form-group">
                        <InputLabel value="Jabatan" />
                        <InputError message={errors.jabatan} />
                        <TextInput name="jabatan" value={datas.jabatan} onChange={(e) => setData('jabatan', e.target.value)} />
                    </div>
                    <div className="form-group">
                        <InputLabel value="Mulai Bekerja" />
                        <InputError message={errors.mulai_bekerja} />
                        <TextInput type="date" name="mulai_bekerja" value={datas.mulai_bekerja} onChange={(e) => setData('mulai_bekerja', e.target.value)} />
                    </div>
                    <div className="form-group">
                        <InputLabel value="Selesai Bekerja" />
                        <InputError message={errors.selesai_bekerja} />
                        <TextInput type="date" name="selesai_bekerja" value={datas.selesai_bekerja} onChange={(e) => setData('selesai_bekerja', e.target.value)} />
                    </div>
                    <div className="form-group">
                        <InputLabel value="Masih Aktif" />
                        <InputError message={errors.masih_aktif} />
                        <FormSelect name="masih_aktif" defaultValue={datas.masih_aktif}  onChange={(e) => setData('masih_aktif', e.target.value)}>
                            <option value="aktif">Ya</option>
                            <option value="tidak">Tidak</option>
                        </FormSelect>
                    </div>
                </div>
            </div>
        </ModalStatic>
    );
}
