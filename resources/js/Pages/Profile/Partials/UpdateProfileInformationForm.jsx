import { Head, useForm, usePage } from '@inertiajs/react';
import InputLabel from '@/Components/InputLabel';
import TextInput from '@/Components/TextInput';
import InputError from '@/Components/InputError';
import PrimaryButton from '@/Components/PrimaryButton';

export default function UpdateProfileInformation() {
    const { auth } = usePage().props

    const { setData, data, processing, progress, post, errors,reset } = useForm({
        role: auth.user.role,
        name: auth.user.name,
        email: auth.user.email,
        username: auth.user.username,
        no_hp: auth.user.no_hp,
        nik: auth.user.nik,
        jabatan: auth.user.jabatan,
        deskripsi: auth.user.deskripsi,
    });

    const handleSubmit = (e) => {
        e.preventDefault();
        post(route('profile.update'),{
            preserveState:true,
            replace:true,
            onSuccess: () => {
                reset()
            }
        });
    };
    
    return (
        <div id="profile" className="policy-item">
            <div className="policy-details">
                <div className="row">
                    <div className="card p-3 ">
                        <div className="d-flex flex-row justify-content-center mb-3">
                            <div className="image"> <img src={auth.user.avatar ?? 'http://127.0.0.1:8000/images/beruang.png'} className="rounded-circle" width={200} height={200} /> <span><i className='bx bxs-camera-plus'></i></span> </div>
                        </div>
                        <div className="d-flex text-center flex-column  justify-content-center ">
                            <h4 className="mb-0">{auth.user.name}</h4>
                            <span>{auth.user.jabatan}</span>
                        </div>
                        <br />
                        <form onSubmit={handleSubmit}>
                            <div className="row">
                                <div className="col-md-6">
                                    <InputLabel value="Nama" />
                                    <InputError message={errors.name}/>
                                    <TextInput name="name" value={data.name} onChange={(e) => setData('name', e.target.value)} />
                                </div>
                                <div className="col-md-6">
                                    <InputLabel value="Email" />
                                    <InputError message={errors.email}/>
                                    <TextInput name="email" value={data.email} onChange={(e) => setData('email', e.target.value)} />
                                </div>
                                <div className="col-md-6">
                                    <InputLabel value="No Handphone" />
                                    <InputError message={errors.no_hp}/>
                                    <TextInput name="no_hp" value={data.no_hp} onChange={(e) => setData('no_hp', e.target.value)} />
                                </div>
                                <div className="col-md-6">
                                    <InputLabel value="Username" />
                                    <InputError message={errors.username}/>
                                    <TextInput name="username" value={data.username} onChange={(e) => setData('username', e.target.value)} />
                                </div>
                                <div className="col-md-6">
                                    <InputLabel value="Jabatan" />
                                    <InputError message={errors.jabatan}/>
                                    <TextInput name="jabatan" value={data.jabatan} onChange={(e) => setData('jabatan', e.target.value)} />
                                </div>
                                <div className="col-md-6">
                                    <InputLabel value="NIK" />
                                    <InputError message={errors.nik}/>
                                    <TextInput name="nik" value={data.nik} onChange={(e) => setData('nik', e.target.value)}/>
                                </div>
                                <div className="col-md-6">
                                    <InputLabel value="Avatar" />
                                    <InputError message={errors.avatar}/>
                                    <TextInput name="avatar" type="file" onChange={(e) => setData('avatar', e.target.files[0])}/>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="about-inputs">
                                        <label>Tentang Anda</label>
                                        <InputError message={errors.deskripsi}/>
                                        <textarea name="deskripsi" className="form-control" type="text" onChange={(e) => setData('deskripsi', e.target.value)} defaultValue={data.deskripsi} />
                                    </div>
                                </div>
                            </div>
                            <div className="mt-3 gap-2 d-flex justify-content-end">
                                <PrimaryButton type='submit' className='btn btn-sm btn-primary' label="Simpan" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}
