import React from 'react';
import {useForm, usePage } from '@inertiajs/react';
import InputLabel from '@/Components/InputLabel';
import TextInput from '@/Components/TextInput';
import InputError from '@/Components/InputError';
import ModalStatic from '@/Components/ModalStatic';
import FormSelect from '@/Components/FormSelect';

export default function UpdatePermission({ datas,onShow,onClose }) {
    const { setData, data, patch, processing, errors,reset } = useForm(datas);
    const { auth ,perusahaan} = usePage().props

    const handleStore = (e) => {
        e.preventDefault();

        patch(route('permission.update'), {
            onSuccess: () => {
                reset(),
                onClose()
            },
            onError: (text) => console.log(text)
        });
    };

    return (
        <ModalStatic show={onShow} handleClose={onClose} onSubmit={handleStore} title="Tambah Hak Akses Perusahaan">
        <div className="row">
            <div className="col">
                <div className="form-group">
                    <InputLabel value="NIK" />
                    <InputError message={errors.nik} />
                    <TextInput name="nik" value={datas.nik} onChange={(e) => setData('nik', e.target.value)} />
                </div>
                <div className="form-group">
                    <InputLabel value="Perusahaan" />
                    <InputError message={errors.id_perusahaan} />
                    <FormSelect name="id_perusahaan" defaultValue={datas.id_perusahaan} onChange={(e) => setData('id_perusahaan', e.target.value)}>
                        <option value="">--Pilih Perushaan--</option>
                         {perusahaan.map((val,key) => {
                                    return (
                                        <option key={key} value={val.id}>{val.name}</option>
                                    )
                                })}
                    </FormSelect>
                </div>
                <div className="form-group">
                    <InputLabel value="Izinkan Perusahaan melihat catatan anda." />
                    <InputError message={errors.akses_edit} />
                    <FormSelect name="akses_edit" defaultValue={datas.akses_edit} onChange={(e) => setData('akses_edit', e.target.value)}>
                        <option value="ya">Ya</option>
                        <option value="tidak">Tidak</option>
                    </FormSelect>
                </div>
                <div className="form-group">
                    <InputLabel value="Izinkan Perusahaan memberikan catatan kepada anda." />
                    <InputError message={errors.akses_view} />
                    <FormSelect name="akses_view" defaultValue={datas.akses_view} onChange={(e) => setData('akses_view', e.target.value)}>
                        <option value="ya">Ya</option>
                        <option value="tidak">Tidak</option>
                    </FormSelect>
                </div>
            </div>
        </div>
    </ModalStatic>
    );
}
