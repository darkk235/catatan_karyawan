import React from 'react';
import { Head, useForm, usePage } from '@inertiajs/react';
import GuestLayout from '@/Layouts/GuestLayout';
import InputLabel from '@/Components/InputLabel';
import TextInput from '@/Components/TextInput';
import InputError from '@/Components/InputError';
import ModalStatic from '@/Components/ModalStatic';
import FormSelect from '@/Components/FormSelect';

export default function StorePermission() {
    const { auth ,perusahaan} = usePage().props
    const [show, setShow] = React.useState(false);

    const { setData, data, post, processing, errors } = useForm({
        nik: auth.user.nik,
        id_perusahaan: "",
        akses_view: "ya",
        akses_edit: "ya",
    });

    const handleStore = (e) => {
        e.preventDefault();

        post(route('permission.store'), {
            onSuccess: () => handleClose(),
            onError: (text) => console.log(text)
        });
    };

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <>
            <div className="title ">
                <h3 className="float-left">Hak Akses Perusahaan</h3>
                <button className='float-right btn-sm btn btn-primary' label="primary" onClick={handleShow}>Tambah</button>
            </div>

            <ModalStatic show={show} handleClose={handleClose} onSubmit={handleStore} title="Tambah Hak Akses Perusahaan">
                <div className="row">
                    <div className="col">
                        <div className="form-group">
                            <InputLabel value="NIK" />
                            <InputError message={errors.nik} />
                            <TextInput name="nik" value={data.nik} onChange={(e) => setData('nik', e.target.value)} />
                        </div>
                        <div className="form-group">
                            <InputLabel value="Perusahaan" />
                            <InputError message={errors.id_perusahaan} />
                            <FormSelect name="id_perusahaan" onChange={(e) => setData('id_perusahaan', e.target.value)}>
                                <option value="">--Pilih Perushaan--</option>
                                {perusahaan.map((val,key) => {
                                    return (
                                        <option key={key} value={val.id}>{val.name}</option>
                                    )
                                })}
                            </FormSelect>
                        </div>
                        <div className="form-group">
                            <InputLabel value="Izinkan Perusahaan melihat catatan anda." />
                            <InputError message={errors.akses_edit} />
                            <FormSelect name="akses_edit" onChange={(e) => setData('akses_edit', e.target.value)}>
                                <option value="ya">Ya</option>
                                <option value="tidak">Tidak</option>
                            </FormSelect>
                        </div>
                        <div className="form-group">
                            <InputLabel value="Izinkan Perusahaan memberikan catatan kepada anda." />
                            <InputError message={errors.akses_view} />
                            <FormSelect name="akses_view" onChange={(e) => setData('akses_view', e.target.value)}>
                                <option value="ya">Ya</option>
                                <option value="tidak">Tidak</option>
                            </FormSelect>
                        </div>
                    </div>
                </div>
            </ModalStatic>
        </>
    );
}
