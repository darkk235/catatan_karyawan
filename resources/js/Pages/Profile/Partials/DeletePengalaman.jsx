import { Head, useForm, usePage } from '@inertiajs/react';
import GuestLayout from '@/Layouts/GuestLayout';
import InputLabel from '@/Components/InputLabel';
import TextInput from '@/Components/TextInput';
import InputError from '@/Components/InputError';
import PrimaryButton from '@/Components/PrimaryButton';
import ModalStatic from '@/Components/ModalStatic';

export default function DeletePengalaman({datas,onShow,onClose}) {
    const { setData, data, processing, progress, delete:destroy, errors } = useForm({
        id: datas.id,
    });

    const handleSubmit = (e) => {
        e.preventDefault();

        destroy(route('pengalaman.destroy'),{
            onSuccess: () => onClose(),
            onError:(text) => console.log(text)
        });
    };

    return (
        <ModalStatic show={onShow} handleClose={onClose} onSubmit={handleSubmit} title="Hapus Pengalaman">
            <div className="row">
                <div className="col">
                    <div className="form-group">
                        <TextInput value={datas.perusahaan} disabled/>
                    </div>
                </div>
            </div>
        </ModalStatic>
    );
}
